import time

from pathlib import Path
from flask import Flask, request, Response, jsonify
from werkzeug.exceptions import BadRequest, NotFound

app = Flask(__name__)

SOLUTIONS_PATH = Path("./solutions")
TASKS_PATH = Path("./tasks")


def get_task(id: str) -> str:
  if id.endswith("_solution"):
    raise NotFound(f"Task {id} does not exist.")

  task_path = TASKS_PATH / id 
  if task_path.exists():
    return open(task_path).read()
    
  raise NotFound(f"Task {id} does not exist.")


def get_solution(id: str) -> str:
  solution_path = TASKS_PATH / (id + "_solution")
  if solution_path.exists():
    return open(solution_path).read()
  raise NotFound(f"Task {id} does not exist.")


def save_solution(id: str, username: str) -> None:
  """ Save timestamp of valid solution w/ username """
  task_solutions_dir = SOLUTIONS_PATH / id
  solution_path = task_solutions_dir / username
  if solution_path.exists():
    raise BadRequest("You already submitted a correct solution for this task.")
  if not task_solutions_dir.exists():
    task_solutions_dir.mkdir(parents=True)
  with open(solution_path, "w+") as f:
    f.write(str(time.time()))


@app.route("/task/<string:id>", methods=["GET", "POST"])
def task_endpoint(id: str) -> Response:
  if request.method == "GET":
    task: str = get_task(id)
    return task

  elif request.method == "POST":
    solution = get_solution(id)
    username = request.args.get("username")
    if not username:
      raise BadRequest("Missing username parameter")
    submission = request.get_data().decode()
    print(f"{submission=}")
    print(f"{solution=}")
    if submission.strip() == solution.strip():
      save_solution(id, username)
      return "You did it! Congrats!"
    return "Sorry, not the right answer..."


@app.route("/scores/<string:id>", methods=["GET"])
def scores(id: str) -> Response:
  """ Return sorted high-score list for some task """
  return jsonify(sorted([
    [path.name, float(open(path).read())]
    for path in (SOLUTIONS_PATH / id).glob("*")
  ], key=lambda t: t[1]))


if __name__ == "__main__":
  app.run("0.0.0.0", port=80)
