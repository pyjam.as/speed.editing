FROM python:3.10-slim-buster

COPY ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

WORKDIR /app
COPY ./ ./

CMD gunicorn --reload --bind 0.0.0.0:1337 main:app
