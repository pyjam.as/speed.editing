from dataclasses import dataclass
import random
from textwrap import dedent
from typing import Callable, List


@dataclass
class Transformer:
  description: str
  function: Callable

  def __call__(self, *args, **kwargs):
    return self.function(*args, **kwargs)


@dataclass
class Task:
  body: str
  transformers: List[Transformer]

  @property
  def description(self):
    return "\n".join("* " + t.description for t in self.transformers)

  def __repr__(self):
    return dedent(f"""\
Transform the text in the following steps:
{self.description}
------------------------------------------
{self.body}
""")

  @property
  def _result(self) -> str:
    result = self.body
    for transformer in self.transformers:
      result = transformer(result)
    return result

  def check_answer(self, answer: str) -> bool:
    return self._result.endswith(answer)


def _every_other_uppercase(text: str) -> str:
  new = []
  for index, word in enumerate(text.split(" ")):
    if index % 2 == 0:
      new.append(word.upper())
    else:
      new.append(word)
  return " ".join(new)


def _comma_seperate_first_10_words(text: str) -> str:
  for symbol in [",", ".", "!", "?"]:
    text = text.replace(symbol, "")
  words = text.split(" ")
  return ', '.join(words[:10]) + ' ' + ' '.join(words[10:])


def _swap_first_and_last(text: str) -> str:
  words = text.split(" ")
  punctuation = ""
  if words[-1].endswith((".", "!", "?")):
    punctuation = words[-1][-1]
    words[-1] = words[-1][:-1]
  words[0], words[-1] = words[-1], words[0]
  return " ".join(words) + punctuation


def _increment_by_1(text: str) -> str:
  words = text.split(" ")
  new = []
  for word in words:
    if word.isdigit():
      word = str(int(word) + 1)
    new.append(word)
  return " ".join(new)
  

def _increment_by_1337(text: str) -> str:
  words = text.split(" ")
  new = []
  for word in words:
    if word.isdigit():
      word = str(int(word) + 1337)
    new.append(word)
  return " ".join(new)


def _uppercase_all_vowels(text: str) -> str:
  return text.replace("a", "A").replace("e", "E").replace("i", "I").replace("o", "O").replace("u", "U").replace("y", "Y")


def _lowercase_all_a(text: str) -> str:
  return text.replace("A", "a")

  
def _lowercase_all_e(text: str) -> str:
  return text.replace("E", "e")


def _reverse_text(text: str) -> str:
  return text[::-1]


def _make_upper_title(text: str) -> str:
  words = text.split(" ")
  new = []
  for word in words:
    if word.isupper():
      word = word.title()
    new.append(word)
  return " ".join(new)


_transformers = [
  Transformer(
    description="Make every other word uppercase, starting from the first.",
    function=_every_other_uppercase,
  ),
  Transformer(
    description="Remove all punctuation and comma seperate the first 10 words.",
    function=_comma_seperate_first_10_words,
  ),
  Transformer(
    description="Swap the first and the last word.",
    function=_swap_first_and_last,
  ),
  Transformer(
    description="Increment all integers by 1.",
    function=_increment_by_1,
  ),
  Transformer(
    description="Increment all integers by 1337.",
    function=_increment_by_1337,
  ),
  Transformer(
    description="Uppercase all vowels.",
    function=_uppercase_all_vowels,
  ),
  Transformer(
    description="Lowercase all A's.",
    function=_lowercase_all_a,
  ),
  Transformer(
    description="Lowercase all E's.",
    function=_lowercase_all_e,
  ),
  Transformer(
    description="Reverse text.",
    function=_reverse_text,
  ),
  Transformer(
    description="Make any uppercase word titlecase",
    function=_make_upper_title,
  ),
]


_bodies = [
  "Distinctively re-engineer revolutionary meta-services and premium architectures. Intrinsically incubate intuitive opportunities and real-time potentialities. Appropriately communicate one-to-one technology after plug-and-play networks.",
  "Quickly aggregate B2B users and worldwide potentialities. Progressively plagiarize resource-leveling e-commerce through resource-leveling core competencies. Dramatically mesh low-risk high-yield alignments before transparent e-tailers.",
  "Appropriately empower dynamic leadership skills after business portals. Globally myocardinate interactive supply chains with distinctive quality vectors. Globally revolutionize global sources through interoperable services.",
  "Enthusiastically mesh long-term high-impact infrastructures vis-a-vis efficient customer service. Professionally fashion wireless leadership rather than prospective experiences. Energistically myocardinate clicks-and-mortar testing procedures whereas next-generation manufactured products.",
  "Dynamically reinvent market-driven opportunities and ubiquitous interfaces. Energistically fabricate an expanded array of niche markets through robust products. Appropriately implement visionary e-services vis-a-vis strategic web-readiness.",
  "Compellingly embrace empowered e-business after user friendly intellectual capital. Interactively actualize front-end processes with effective convergence. Synergistically deliver performance based methods of empowerment whereas distributed expertise.",
  "Efficiently enable enabled sources and cost effective products. Completely synthesize principle-centered information after ethical communities. Efficiently innovate open-source infrastructures via inexpensive materials.",
  "Objectively integrate enterprise-wide strategic theme areas with functionalized infrastructures. Interactively productize premium technologies whereas interdependent quality vectors. Rapaciously utilize enterprise experiences via 24/7 markets.",
  "Uniquely matrix economically sound value through cooperative technology. Competently parallel task fully researched data and enterprise process improvements. Collaboratively expedite quality manufactured products via client-focused results.",
  "Quickly communicate enabled technology and turnkey leadership skills. Uniquely enable accurate supply chains rather than frictionless technology. Globally network focused materials vis-a-vis cost effective manufactured products.",
  "Enthusiastically leverage existing premium quality vectors with enterprise-wide innovation. Phosfluorescently leverage others enterprise-wide 'outside the box' thinking with e-business collaboration and idea-sharing. Proactively leverage other resource-leveling convergence rather than inter-mandated networks.",
  "Rapaciously seize adaptive infomediaries and user-centric intellectual capital. Collaboratively unleash market-driven 'outside the box' thinking for long-term high-impact solutions. Enthusiastically engage fully tested process improvements before top-line platforms."
]


def _random_body() -> str:
  return random.choice(_bodies)


def _random_transformer_list() -> List[Transformer]:
  k = max(2, int(random.gauss(4, 1)))
  return random.sample(_transformers, k)


def create_random_task() -> Task:
  return Task(
    body=_random_body(),
    transformers=_random_transformer_list(),
  )
  

t = create_random_task()
print(t)
print("\n\nResult:")
print(t._result)